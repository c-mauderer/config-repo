#!/usr/bin/false
# This is a command set file and is not intended to be executed directly

[[ "$MY_SYSTEM" == "" ]] && printerror "System not set."
[[ "$COMMANDSETDIR" == "" ]] && printerror "Commandset directory not set."

# determine config base dir
CONFDIR="${COMMANDSETDIR}/configs"
print_msg 1 "Config dir: ${CONFDIR}"

# VIM
check_and_link "${CONFDIR}/vim/.vim" "${HOME}/.vim"
check_and_link "${CONFDIR}/vim/.vimrc" "${HOME}/.vimrc"
check_and_link "${CONFDIR}/vim/.vim_include" "${HOME}/.vim_include"

# NVIM
check_and_link "${CONFDIR}/nvim/nvim" "${HOME}/.config/nvim"

# GIT
[[ $MY_SYSTEM == privat* ]] && check_and_link "${CONFDIR}/git/.gitconfig_privat" "${HOME}/.gitconfig"
[[ $MY_SYSTEM == work* ]] && check_and_link "${CONFDIR}/git/.gitconfig_work" "${HOME}/.gitconfig"
check_and_link "${CONFDIR}/git/.gitignore_global" "${HOME}/.gitignore_global"
check_and_link "${CONFDIR}/git/.gitattributes_global" "${HOME}/.gitattributes_global"

# GDB
check_and_link "${CONFDIR}/gdb/.gdbinit" "${HOME}/.gdbinit"

# ZSH
check_and_link "${CONFDIR}/zsh/.zshrc" "${HOME}/.zshrc"
check_and_link "${CONFDIR}/zsh/.zshrc.zni" "${HOME}/.zshrc.zni"
check_and_link "${CONFDIR}/zsh/.zsh" "${HOME}/.zsh"

# TMUX
check_and_link "${CONFDIR}/tmux/.tmux.conf" "${HOME}/.tmux.conf"
check_and_link "${CONFDIR}/tmux/.tmux" "${HOME}/.tmux"

# SCREEN
check_and_link "${CONFDIR}/screen/.screenrc" "${HOME}/.screenrc"

# SLIC3R
check_and_link "${CONFDIR}/slic3r/.Slic3r" "${HOME}/.Slic3r"

# SUPERSLICER
check_and_link "${CONFDIR}/superslicer/SuperSlicer" "${HOME}/.config/SuperSlicer"

# PRUSASLICER
check_and_link "${CONFDIR}/prusaslicer/PrusaSlicer" "${HOME}/.config/PrusaSlicer"

# INPUTRC
check_and_link "${CONFDIR}/inputrc/.inputrc" "${HOME}/.inputrc"

# PULSE
check_and_link "${CONFDIR}/pulse/daemon.conf" "${HOME}/.config/pulse/daemon.conf"

# KONSOLE
check_and_link "${CONFDIR}/konsole/konsole" "${HOME}/.local/share/konsole"
check_and_link "${CONFDIR}/konsole/konsolerc" "${HOME}/.config/konsolerc"

# XFCE4 Terminal
check_and_link "${CONFDIR}/xfce4/terminal" "${HOME}/.config/xfce4/terminal"

# PRONTERFACE
check_and_link "${CONFDIR}/pronterface/.pronsolerc" "${HOME}/.pronsolerc"

# WALLET
check_and_link "${CONFDIR}/kdewallet/kwalletrc" "${HOME}/.config/kwalletrc"
check_and_link "${CONFDIR}/kdewallet/kwalletrc" "${HOME}/.kde4/share/config/kwalletrc"

# WALLET
check_and_link "${CONFDIR}/maxima/.maxima" "${HOME}/.maxima"

# ONBOARD
check_and_link "${CONFDIR}/onboard/onboard" "${HOME}/.local/share/onboard"

# LATEX
check_and_link "${CONFDIR}/latex/.latexmkrc" "${HOME}/.latexmkrc"
