#!/usr/bin/env bash

set -e
set -u
set -o pipefail

LOG="update-links-${HOSTNAME}.log"

if [ ! -z ${1+x} ]
then
	echo "Init subrepositories and execute update-links.sh."
	exit 1
fi

git submodule init
git submodule sync
git submodule update

date >> "$LOG"
./update-links.sh -vb configs-links.sh 2>&1 | tee -a "$LOG"
