#!/usr/bin/env bash

# Copyright (c) 2015 Christian Mauderer <oss@c-mauderer.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# exit on wrong command and undefined variable
set -e
set -u
# The following one only works for bash. It catches the fail of the first
# command in a pipe. e.g. 'which asdf | true' would fail due to the missing asdf
# command.
set -o pipefail

VERBOSE=0
BACKUP=0
BACKUP_POSTFIX=".config_backup_`date +%Y%m%d_%H%M%S`"

MY_SYSTEM="unknown"

SCRIPTNAME=$0

# Print a error message and exits with 1
# Arguments:
#	0: message
printerror () {
	echo "ERROR: $1"
	echo ""
	echo "Call with \"${SCRIPTNAME} -h\" for help."
	exit 1
}

# Check if the parameter is a program. Print error and exit if not.
# Arguments:
#	0: program name
test_command () {
	if ! which $1 >/dev/null 2>&1
	then
		printerror "Command \"$1\" not found."
	fi
}

# Print a message depending on verbose level (set in ${VERBOSE}).
# Arguments:
#	0: message will be printed on this level
#	1: message
print_msg () {
	local LEVEL="$1"
	local MSG="$2"

	if [ ${LEVEL} -eq 0 ]
	then
		echo "${MSG}"
	else
		if [ ${LEVEL} -le ${VERBOSE} ]
		then
			echo "${MSG}" 1>&2
		fi
	fi
}

# Print a help text and exit
# Arguments:
#	n.a.
printhelp () {
	echo ""
	echo "Call:   ${SCRIPTNAME} [<options>] <commandset>"
	echo "Create or update links to my configuration."
	echo ""
	echo "The following parameters are optional:"
	echo "  -b          Back up existing files instead of failing."
	echo "  -h          Print this help and exit the script."
	echo "  -v          Be more verbose. Can be used multiple times."
	exit 0
}

# Check for the existence of a file and create link
# Arguments:
#	0: file to link to
#	1: link name
check_and_link () {
	local TARGET="$1"
	local LINK="$2"
	local LINKDIR=`dirname "${LINK}"`

	print_msg 1 "Creating link \"$LINK\" pointing to \"$TARGET\""

	print_msg 2 "Check for existence of \"$LINK\""
	if [ -e "$LINK" -o -L "$LINK" ]
	then
		print_msg 2 "\"$LINK\" exists."

		if [ -L "$LINK" ]
		then
			print_msg 2 "\"$LINK\" is a link. Remove it."
			print_msg 0 "old: \"`ls -l "$LINK"`\""
			rm "$LINK"
		else
			if [ $BACKUP -ne 0 ]
			then
				print_msg 1 "\"$LINK\" exists. Make backup."
				local BU="${LINK}${BACKUP_POSTFIX}"
				print_msg 0 "backup: \"$LINK\" to \"$BU\""
				mv "${LINK}" "${BU}"
			else
				printerror "File \"$LINK\" exists."
			fi
		fi
	fi

	print_msg 2 "Create \"$LINKDIR\" if necessary."
	mkdir -p "${LINKDIR}"

	ln -s "${TARGET}" "${LINK}"
	print_msg 0 "new: \"`ls -l "$LINK"`\""
}

# The main script

# Read options
while getopts "bhv" OPTION
do
	case ${OPTION} in
		b)  BACKUP=1 ;;
		h)  printhelp ;;
		v)  VERBOSE=$((${VERBOSE} + 1)) ;;
		\?) printerror "Unknown option \"-${OPTARG}\"." ;;
		:)  printerror "Option \"-${OPTARG}\" needs an argument." ;;
	esac
done
# Remove the already processed ones
shift $((${OPTIND} - 1))

[[ $VERBOSE -ge 3 ]] && set -x

print_msg 1 "Verbose level: ${VERBOSE}"

# Check for command set to use
[[ $# -ne 1 ]] && printerror "Need a command set."

COMMANDSET="$1"

[[ -e "$COMMANDSET" ]] || printerror "Commandset \"$COMMANDSET\" not found."

# find out commandset directory
COMMANDSETDIR="$( cd "$( dirname -- "${COMMANDSET}" )" && pwd -P )"
print_msg 1 "Script dir: ${COMMANDSETDIR}"

[[ $HOSTNAME == dia* ]] && MY_SYSTEM="privat-srv-linux"
[[ $HOSTNAME == christian-pc* ]] && MY_SYSTEM="privat-pc-linux"
[[ $HOSTNAME == christian-x230i* ]] && MY_SYSTEM="privat-nb-linux"
[[ $HOSTNAME == christian-inspiron* ]] && MY_SYSTEM="privat-nb-linux"
[[ $HOSTNAME == christian-surftab* ]] && MY_SYSTEM="privat-tablet-linux"
[[ $HOSTNAME == christian-tab* ]] && MY_SYSTEM="privat-tablet-linux"
[[ $HOSTNAME == samsung-gt510* ]] && MY_SYSTEM="privat-tablet-linux"
[[ $HOSTNAME == synology* ]] && MY_SYSTEM="privat-nas-linux"
[[ $HOSTNAME == mauderer-linux.eb.localhost ]] && MY_SYSTEM="work-nb-linux"
[[ $HOSTNAME == mauderer-nb-li* ]] && MY_SYSTEM="work-nb-linux"
[[ $HOSTNAME == MAUDERER-NB-LI* ]] && MY_SYSTEM="work-nb-linux"
[[ $HOSTNAME == MAUDERER-TUX* ]] && MY_SYSTEM="work-nb-linux"
[[ $HOSTNAME == mauderer2-nb-w* ]] && MY_SYSTEM="work-nb-windows"
[[ $HOSTNAME == lupus ]] && MY_SYSTEM="work-lupus"
[[ $HOSTNAME == labor-nb-li ]] && MY_SYSTEM="work-labor"

print_msg 1 "Use configuration for $MY_SYSTEM"

# Check necessary dirs
[[ "${HOME}" = "" ]] && printerror "HOME not set"

# source commandset
export MY_SYSTEM
export COMMANDSETDIR
export HOME
source "$COMMANDSET"

# vim: set ts=4 sw=4:
