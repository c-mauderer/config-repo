" Vim syntax file
" Language:     creole + m4
" Maintainer:   oss@c-mauderer.de
" Last Change:  2014-08-27

if exists("b:current_syntax")
    finish
endif

runtime! syntax/creole.vim
if exists("b:current_syntax")
    unlet b:current_syntax
endif

runtime! syntax/m4.vim
if exists("b:current_syntax")
    unlet b:current_syntax
endif

let b:current_syntax = "mct"

" vim: set ts=4 sw=4 et:
