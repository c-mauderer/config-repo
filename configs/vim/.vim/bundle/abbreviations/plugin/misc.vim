iabbrev /**vim**/ /* vim: set ts=4 sw=4: */
iabbrev ##vim## # vim: set ts=4 sw=4:

iabbrev #ir# #ifdef __rtems__
iabbrev #inr# #ifndef __rtems__
iabbrev #er# #else /* __rtems__ */
iabbrev #eir# #endif /* __rtems__ */

iabbrev #ip# #ifdef PGH
iabbrev #inp# #ifndef PGH
iabbrev #ep# #else /* PGH */
iabbrev #eip# #endif /* PGH */

iabbrev emltrenner -=-=-=-=-=-=-=-=-=# Diese Zeile unbedingt stehen lassen - unterhalb beginnen Sie Ihren Mailtext #=-=-=-=-=-=-=-=-=-
