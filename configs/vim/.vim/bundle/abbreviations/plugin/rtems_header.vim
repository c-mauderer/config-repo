iabbrev ##RTEMS## # SPDX-License-Identifier: BSD-2-Clause
\<CR>#
\<CR># Copyright (C) 2025 embedded brains GmbH & Co. KG
\<CR>#
\<CR># Redistribution and use in source and binary forms, with or without
\<CR># modification, are permitted provided that the following conditions
\<CR># are met:
\<CR># 1. Redistributions of source code must retain the above copyright
\<CR>#    notice, this list of conditions and the following disclaimer.
\<CR># 2. Redistributions in binary form must reproduce the above copyright
\<CR>#    notice, this list of conditions and the following disclaimer in the
\<CR>#    documentation and/or other materials provided with the distribution.
\<CR>#
\<CR># THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
\<CR># AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
\<CR># IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
\<CR># ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
\<CR># LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
\<CR># CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
\<CR># SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
\<CR># INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
\<CR># CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
\<CR># ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
\<CR># POSSIBILITY OF SUCH DAMAGE.

iabbrev /**RTEMS**/ /* SPDX-License-Identifier: BSD-2-Clause */
\<CR>
\<CR>/**
\<CR>@file
\<CR>
\<CR>@ingroup RTEMSXxxYyy
\<CR>
\<CR>@brief This file provides the implementation of @ref RTEMSXxxYyy.
\<CR><BS>/
\<CR>
\<CR>/*
\<CR>Copyright (C) 2025 embedded brains GmbH & Co. KG
\<CR><BS>
\<CR>Redistribution and use in source and binary forms, with or without
\<CR>modification, are permitted provided that the following conditions
\<CR>are met:
\<CR>1. Redistributions of source code must retain the above copyright
\<CR>   notice, this list of conditions and the following disclaimer.
\<CR><BS><BS><BS>2. Redistributions in binary form must reproduce the above copyright
\<CR>   notice, this list of conditions and the following disclaimer in the
\<CR>documentation and/or other materials provided with the distribution.
\<CR><BS><BS><BS>
\<CR>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
\<CR>AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
\<CR>IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
\<CR>ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
\<CR>LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
\<CR>CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
\<CR>SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
\<CR>INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
\<CR>CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
\<CR>ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
\<CR>POSSIBILITY OF SUCH DAMAGE.
\<CR>/

iabbrev /**RTEMS_H**/ /* SPDX-License-Identifier: BSD-2-Clause */
\<CR>
\<CR>/**
\<CR>@file
\<CR>
\<CR>@ingroup RTEMSXxxYyy
\<CR>
\<CR>@brief This file provides the interfaces of @ref RTEMSXxxYyy.
\<CR><BS>/
\<CR>
\<CR>/*
\<CR>Copyright (C) 2025 embedded brains GmbH & Co. KG
\<CR><BS>
\<CR>Redistribution and use in source and binary forms, with or without
\<CR>modification, are permitted provided that the following conditions
\<CR>are met:
\<CR>1. Redistributions of source code must retain the above copyright
\<CR>   notice, this list of conditions and the following disclaimer.
\<CR><BS><BS><BS>2. Redistributions in binary form must reproduce the above copyright
\<CR>   notice, this list of conditions and the following disclaimer in the
\<CR>documentation and/or other materials provided with the distribution.
\<CR><BS><BS><BS>
\<CR>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
\<CR>AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
\<CR>IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
\<CR>ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
\<CR>LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
\<CR>CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
\<CR>SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
\<CR>INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
\<CR>CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
\<CR>ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
\<CR>POSSIBILITY OF SUCH DAMAGE.
\<CR>/
\<CR>
\<CR>#ifndef XXX_H
\<CR>#define XXX_H
\<CR>
\<CR>#include <rtems.h>
\<CR>
\<CR>#ifdef __cplusplus
\<CR>extern "C" {
\<CR>#endif /* __cplusplus */
\<CR>
\<CR><BS>/**
\<CR>@defgroup RTEMSXxxYyy
\<CR>
\<CR>@ingroup RTEMSXxx
\<CR>
\<CR>@brief XXX
\<CR>
\<CR>YYYYYY
\<CR>
\<CR>@{
\<CR><BS>/
\<CR>
\<CR>
\<CR>
\<CR><BS>/** @} */
\<CR>
\<CR>#ifdef __cplusplus
\<CR>}
\<CR>#endif /* __cplusplus */
\<CR>
\<CR>#endif /* XXX_H */
