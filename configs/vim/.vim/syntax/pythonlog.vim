" Vim syntax file
" Language: python logger log files
" Maintainer: Christian Mauderer

" Tailored for default format with DEBUG INFO, ... at the beginning of the line

" quit when a syntax file was already loaded
if exists("b:current_syntax")
	finish
endif

syn match pythonlogLine		"^"		nextgroup=pythonlogLevelDEBUG,pythonlogLevelINFO,pythonlogLevelWARNING,pythonlogLevelERROR
syn match pythonlogLevelDEBUG	"^DEBUG:.*"	contains=pythonlogLine
syn match pythonlogLevelINFO	"^INFO:.*"	contains=pythonlogLine
syn match pythonlogLevelWARNING	"^WARNING:.*"	contains=pythonlogLine
syn match pythonlogLevelERROR	"^ERROR:.*"	contains=pythonlogLine

" Keywords
hi pythonlogLevelDEBUG		ctermfg=darkgray	guifg=darkgray
hi pythonlogLevelINFO		ctermfg=darkgreen	guifg=darkgreen
hi pythonlogLevelWARNING	ctermfg=darkyellow	guifg=darkyellow
hi pythonlogLevelERROR		ctermfg=red		guifg=red
