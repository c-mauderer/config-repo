" Vim syntax file
" Language: python logger log files
" Maintainer: Christian Mauderer

" Tailored for the following format:
"     fmt='%(asctime)s:%(levelname)s:%(name)s: %(message)s'
"     datefmt = '%Y%m%d-%H%M%S'
" Example line:
"     20191016-145857:INFO:my.cool.module: Some Info

" quit when a syntax file was already loaded
if exists("b:current_syntax")
	finish
endif

syn match pythonlogLine		"^"			nextgroup=pythonlogDate
syn match pythonlogDate		"^.\{-}:"		contains=pythonlogLine nextgroup=pythonlogLevelDEBUG,pythonlogLevelINFO,pythonlogLevelWARNING,pythonlogLevelERROR
syn match pythonlogLevelDEBUG	"^.\{-}:DEBUG:.*"	contains=pythonlogDate nextgroup=pythonlogName
syn match pythonlogLevelINFO	"^.\{-}:INFO:.*"	contains=pythonlogDate nextgroup=pythonlogName
syn match pythonlogLevelWARNING	"^.\{-}:WARNING:.*"	contains=pythonlogDate nextgroup=pythonlogName
syn match pythonlogLevelERROR	"^.\{-}:ERROR:.*"	contains=pythonlogDate nextgroup=pythonlogName
" syn match pythonlogName		".\{-}:"		contained

" Keywords
hi pythonlogDate		ctermfg=darkblue	guifg=darkblue
hi pythonlogLevelDEBUG		ctermfg=darkgray	guifg=darkgray
hi pythonlogLevelINFO		ctermfg=darkgreen	guifg=darkgreen
hi pythonlogLevelWARNING	ctermfg=darkyellow	guifg=darkyellow
hi pythonlogLevelERROR		ctermfg=red		guifg=red
" hi pythonlogName		ctermfg=darkblue	guifg=darkblue
