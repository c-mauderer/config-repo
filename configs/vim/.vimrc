" Some special case when to use more colors

" Hack for nvim: Background not detected correctly
if has('nvim-0.4')
	if $TMUX != ""
		set background=dark
	endif
endif

" use pathogen
" https://github.com/tpope/vim-pathogen
" NOTE: eventually switch to vundle in the future?
" to recreate helptags execute
"     vim -c 'call pathogen#helptags()|q'
if has("win32")
else
	execute pathogen#infect()
endif

" enable "not fully compatible with vi" mode
set nocompatible

" larger history
set history=10000

" no two spaces after end of sentence
set nojoinspaces

" fix Backspace key
set backspace=indent,eol,start

" enable line numbers
set nu!

" enable folding
set fdm=marker

" disable modeline - I don't want settings from other people
set nomodeline
set modelines=0

" disable bell completely
set visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Enable mouse in console
if ! has('gui_running')
	set mouse=a
endif
set pastetoggle=<F2>

" Better mouse for TMUX
if $TMUX != ""
	if !has('nvim')
		set ttymouse=xterm2
	endif
endif

" setup statusline
" allways on
set laststatus=2
" colors
hi User1 gui=bold guifg=#c0a36e guibg=#2c4577 cterm=bold ctermfg=227 ctermbg=239
hi User2 gui=bold guifg=#98bb6c guibg=#2c4577 cterm=bold ctermfg=47 ctermbg=239
hi User3 gui=bold guifg=#c34043 guibg=#2c4577 cterm=bold ctermfg=197 ctermbg=239
hi User4 gui=bold guifg=#dcd7ba guibg=#2c4577 cterm=bold ctermfg=231 ctermbg=239
hi User5 gui=bold guifg=#938aa9 guibg=#2c4577 cterm=bold ctermfg=75 ctermbg=239
set statusline+=%4*%n
set statusline+=%1*\ %y[%{&ff}]
set statusline+=%<
set statusline+=%2*\ %f
set statusline+=%3*\ %q%h%m%r%w
set statusline+=%=
set statusline+=%4*%-17.(l:%l/%L\ c:%v%)
set statusline+=%5*\ chr:%02B

" Some more color settings
if has('gui_running')
	set background=dark
endif

" set backups on and store them in ~/tmp/
set bk
set backupdir=~/tmp/

" hide buffers instead of closing them if they are abandoned
set hidden

" enable spellchecking (GUI only)
if has('gui_running') || &t_Co >= 256
	if has('win32')
		set spell spelllang=en
		set spellfile=~/_vim/spellfile.add
	else
		if has('nvim')
			set spell spelllang=de,en
			set spellfile=~/.vim/spellfile.add
		endif
	endif
	if has('nvim')
		hi SpellBad   ctermbg=None cterm=undercurl gui=undercurl guisp=DarkRed
		hi SpellCap   ctermbg=None cterm=undercurl gui=undercurl guisp=SlateBlue
		hi SpellRare  ctermbg=None cterm=undercurl gui=undercurl guisp=DarkMagenta
		hi SpellLocal ctermbg=None cterm=undercurl gui=undercurl guisp=DarkCyan
	else
		hi SpellBad   ctermbg=052 gui=undercurl guisp=Red
		hi SpellCap   ctermbg=052 gui=undercurl guisp=Blue
		hi SpellRare  ctermbg=None gui=undercurl guisp=Magenta
		hi SpellLocal ctermbg=None gui=undercurl guisp=DarkCyan
	endif
endif

" Syntax highlighting
syn on
set hls
au BufRead,BufNewFile *.dox set filetype=cpp.doxygen
let perl_include_pod=1

" Enable filetype plugins
filetype plugin on

" Set completion behaviour for opening files
set wildmode=longest,list,full
set wildmenu

" display options
if match(hostname(), 'christian-x230i') == 0
	set guifont=DejaVu\ Sans\ Mono\ 9
elseif match(hostname(), 'mauderer-nb-li') == 0
	set guifont=DejaVu\ Sans\ Mono\ 10
elseif has('win32')
	set guifont=Lucida_Console:h11:cANSI
else
	set guifont=DejaVu\ Sans\ Mono\ 10
endif

" some printing options
if ! has('nvim')
	set printoptions=paper:A4,left:10mm,right:10mm,top:10mm,bottom:10mm,number:y
	set printfont=DejaVu\ Sans\ Mono:h10
endif

" Put a grey line on column 80
set textwidth=80
set colorcolumn=+1
set fo-=t

" enable listing of spaces at the beginning or end of line and tabs
set list
if has('win32')
	set listchars=tab:>·,trail:·
	if has('nvim')
		hi Whitespace ctermfg=white
	else
		hi SpecialKey guifg=lightgrey ctermfg=LightGray
	endif
else
	set listchars=tab:→·,trail:·
	if has('nvim')
		hi Whitespace ctermfg=239
	else
		hi SpecialKey guifg=#494d4b ctermfg=239
	endif
	" enter characters with "^k - >" and "^k . M" (or "^k 0 a")
	" See ":digraphs" for a complete list
endif

" Autoindent
set autoindent

" lines for vim latexsuite
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

" Vimwiki settings
let g:vimwiki_list = [{'path': '~/Nextcloud/vimwiki/'}]
let g:vimwiki_ext2syntax = {'.wiki': 'default',
			\ '.mdwiki': 'markdown'}

" Automatically open, but do not go to (if there are errors) the quickfix /
" location list window, or close it when is has become empty.
" https://vim.fandom.com/wiki/Automatically_open_the_quickfix_window_on_:make
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" Incrementing selected numbers
" see http://vim.wikia.com/wiki/Making_a_list_of_numbers
if v:version < 800
	function! Incr()
		let a = line('.') - line("'<")
		let c = virtcol("'<")
		if a > 0
			execute 'normal! '.c.'|'.a."\<C-a>"
		endif
		normal `<
	endfunction
	vnoremap g<C-a> :call Incr()<CR>
endif

" Add all "tags"-files to the tags variable
function s:search_and_add_tags(path)
	let s:tag_files = findfile("tags", fnameescape(a:path) . ";", -1)
	for s:tag_file in s:tag_files
		if filereadable(s:tag_file)
	"		echomsg "Add the following file to tags: " . fnameescape(s:tag_file)
			exec "set tags+=" . fnameescape(fnameescape(s:tag_file))
		endif
	endfor
endfunction

" Function for sourcing a file with given filename. It searches for it from
" given path upwards towards the root directory.
"
" Example:
" Search for ".vim_include" from the directory of the current file on upwards:
" 	call s:search_and_source_file(expand("%:p:h"), ".vim_include")
function s:search_and_source_file(path, filename)
	let s:source = findfile(fnameescape(a:filename), fnameescape(a:path) . ";")
	if filereadable(s:source)
"		echomsg "source the following file: " . fnameescape(s:source)
		exec "source " . fnameescape(s:source)
	endif
endfunction

" Note: I should start to use editorconfig.org instead
autocmd BufRead,BufNewFile .vim_include set filetype=vim
autocmd BufRead,BufNewFile * call s:search_and_source_file(expand("%:p:h"), ".vim_include")
autocmd BufRead,BufNewFile * call s:search_and_add_tags(expand("%:p:h"))

call s:search_and_add_tags(getcwd())

" some other syntax formats
autocmd BufRead,BufNewFile *.t2t set filetype=txt2tags_own
autocmd BufRead,BufNewFile linkcmds* set filetype=ld
autocmd BufRead,BufNewFile *.gdb set filetype=gdb
autocmd BufRead,BufNewFile *.j2 set filetype=jinja
autocmd BufRead,BufNewFile *.srec set filetype=srec
autocmd BufRead,BufNewFile *.ino set filetype=cpp
autocmd BufRead,BufNewFile *.tmux set filetype=tmux
autocmd BufRead,BufNewFile *.dtso set filetype=dts
autocmd BufRead,BufNewFile wscript set filetype=python

autocmd BufRead,BufNewFile *.vhd set comments=:--!,:--
autocmd BufRead,BufNewFile *.vhd set formatoptions=cqr
autocmd BufRead,BufNewFile *.vhd set et ts=4

autocmd BufRead,BufNewFile *COMMIT_EDITMSG set textwidth=72
autocmd BufRead,BufNewFile .gitsendemail.msg.* set textwidth=72

" vim "make" interprets times followed by something as an error in a line.
" That is annoying for waf which prints information like
"     11:16:53 runner [...
" That would be interpreted as an error in file "11" in line "16".
" The following pattern ignores these
" set errorformat^=%-G%f:%l:\\d\\d\ runner\ [

" Append ifdef syntax
function s:append_ifdef_syntax()
	if exists('b:current_syntax')
		if b:current_syntax == 'c'
			set filetype=c.ifdef
		endif
		if b:current_syntax == 'cpp'
			set filetype=cpp.ifdef
		endif
		Define __rtems__
	endif
endfunction
autocmd BufRead,BufNewFile *.c call s:append_ifdef_syntax()

" Some shortcuts
imap <F3> <C-R>=strftime("%d.%m.%Y")<CR>
imap <S-F3> <C-R>=strftime("%Y-%m-%d %H:%M:%S")<CR>
imap <F4> <C-R>="Christian Mauderer"<CR>
imap <S-F4> <C-R>="Christian Mauderer <christian.mauderer@embedded-brains.de>"<CR>

" Use <Esc> to exit terminal-mode in nvim
if has('nvim')
	tnoremap <Esc> <C-\><C-n>
endif

" URL decode command
:command -range URLDecode <line1>,<line2>s/%\(\x\x\)/\=iconv(nr2char('0x' .. submatch(1)), 'utf-8', 'latin1')/ge
