-- Language Server Protocol (LSP)
local lspconfig = require('lspconfig')
-- LSP: Languagetool
lspconfig.ltex.setup {
  cmd = { "ltex-ls" },
  -- flags = { debounce_text_changes = 300 },
  settings = {
    -- see https://valentjn.github.io/ltex/settings.html
    ltex = {
      -- note: Exact language like "de_DE" or "en_US" would be better. But
      -- doesn't work for multiple languages.
      language = "auto",
      enabled = true,
    }
  }
}
-- LSP: clangd
-- note: use `bear -- <compile command>` in the project root to generate
-- necessary project config
lspconfig.clangd.setup {
}
-- LSP: python (ruff)
lspconfig.ruff.setup {
}
