-- Color Scheme
require('kanagawa').setup({
  transparent = true,
  background = {
    dark = "wave",
    light = "lotus"
  },
  colors = {
    palette = {
      -- Use colors from dragon
      sumiInk0 = "#0d0c0c",
      sumiInk1 = "#12120f",
      sumiInk2 = "#1D1C19",
      sumiInk3 = "#181616",
      sumiInk4 = "#282727",
      sumiInk5 = "#393836",
      sumiInk6 = "#625e5a",
    },
    theme = {
      wave = {
        ui = {
          bg_m3 = "#2c4577",
          bg_visual = "#2d4f67",
        },
      },
    },
  },
})
vim.cmd("colorscheme kanagawa")
