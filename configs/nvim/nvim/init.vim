set runtimepath=
set runtimepath+=~/.config/nvim
set runtimepath+=~/.vim
set runtimepath+=~/.vim/after
set runtimepath+=/usr/local/share/nvim/site
set runtimepath+=/usr/share/nvim/site
set runtimepath+=/usr/share/nvim/runtime
set runtimepath+=/usr/share/nvim/runtime/pack/dist/opt/matchit
set runtimepath+=/usr/lib/nvim
set runtimepath+=/usr/share/nvim/site/after
set runtimepath+=/usr/local/share/nvim/site/after
let &packpath = &runtimepath
lua require('config-pre')
source ~/.vimrc
lua require('config-post')
map <space>e :lua vim.diagnostic.open_float(0, {scope="line"})<CR>
