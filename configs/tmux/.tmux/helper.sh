#!/usr/bin/env false

# NOTE: This file should not be called directly. Instead source it and use the
# functions.

is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"

# Cache the TMUX version for speed.
tmux_version="$(tmux -V | cut -d ' ' -f 2)"
if [ "${tmux_version}" != "master" ]
then
	tmux_version_mayor="$(echo ${tmux_version} | cut -d '.' -f 1)"
	tmux_version_minor="$(echo ${tmux_version} | cut -d '.' -f 2)"
fi

# To check for example for 2.4 use
#    tmux_is_at_least 2 4
tmux_is_at_least() {
	if [ "${tmux_version}" == "master" ]
	then
		return 0
	fi

	if [ $tmux_version_mayor -lt $1 ]
	then
		return 1
	fi

	if [ $tmux_version_minor -lt $2 ]
	then
		return 1
	fi

	return 0
}
