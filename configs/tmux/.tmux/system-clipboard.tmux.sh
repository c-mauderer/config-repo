#!/usr/bin/env bash

SCRIPTDIR=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)
source ${SCRIPTDIR}/helper.sh

if ! tmux_is_at_least 2 4
then
	echo "Unsupported TMUX version"
	exit 1
fi

uname | grep -qi Darwin \
    && which reattach-to-user-namespace > /dev/null \
    && SYSTEM="Mac"
uname | grep -qi Linux \
    && which xclip > /dev/null \
    && SYSTEM="Linux"
uname | grep -qi Cygwin \
    && SYSTEM="Cygwin"

# command to copy from stdin to clipboard
COPYCMD=""
# command to copy from clipboard to stdout
PASTECMD=""

case "${SYSTEM}" in
"Mac")
	COPYCMD="reattach-to-user-namespace pbcopy"
	PASTECMD="reattach-to-user-namespace pbpaste"
	;;
"Linux")
	COPYCMD="xclip -i -sel p -f | xclip -i -sel c "
	PASTECMD="xclip -o"
	;;
"Cygwin")
	COPYCMD="cat > /dev/clipboard"
	PASTECMD="cat /dev/clipboard"
	;;
esac

# Key bindings
if [ "${COPYCMD}" != "" ]
then
	tmux bind-key -T copy-mode    C-w               send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode    MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode    M-w               send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode-vi 'y'               send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode-vi Enter             send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "$COPYCMD"
	tmux bind-key -T copy-mode-vi DoubleClick1Pane  select-pane \\\; send-keys -X select-word \\\; send-keys -X copy-pipe "$COPYCMD"
	tmux bind-key -T root         DoubleClick1Pane  if-shell "$is_vim" "send-keys -M" "select-pane ; copy-mode -M ; send-keys -X select-word ; run-shell \"sleep 0.2\" ; send-keys -X copy-pipe-and-cancel \"$COPYCMD\""
fi
if [ "${PASTECMD}" != "" ]
then
	tmux bind-key -T prefix ']'            run "${PASTECMD} | tmux load-buffer - ; tmux paste-buffer"
	tmux bind-key -T root   MouseDown2Pane run "${PASTECMD} | tmux load-buffer - ; tmux paste-buffer"
fi
