[Appearance]
ColorScheme=Solarized
Font=DejaVu Sans Mono,10,-1,5,50,0,0,0,0,0

[General]
LocalTabTitleFormat=%d : %n %w
Name=Darkshell
Parent=FALLBACK/

[Interaction Options]
WordCharacters=@-./_~?&=%+#:

[Scrolling]
HistorySize=500000
