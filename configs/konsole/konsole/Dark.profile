[Appearance]
ColorScheme=Linux

[General]
LocalTabTitleFormat=%d : %n %w
Name=Dark
Parent=FALLBACK/

[Scrolling]
HistorySize=100000
