[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Hell auf dunkel
Font=Source Code Pro,11,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[General]
LocalTabTitleFormat=%d : %n %w
Name=Shell
Parent=FALLBACK/

[Interaction Options]
WordCharacters=@-./_~?&=%+#:

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=1
HistorySize=500000
