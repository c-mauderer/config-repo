########## completion
zstyle ':completion:*' completer _expand _complete _ignored _match _correct _prefix _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' matcher-list '' '+m:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' original false
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*' accept-exact false
zstyle ':completion:*' rehash true
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn hg
if [ -e $HOME/.zsh/plugins/zsh-async/async.zsh ]
then
	# asynchronous vcs info
	# based on https://github.com/vincentbernat/zshrc/blob/d66fd6b6ea5b3c899efb7f36141e3c8eb7ce348b/rc/vcs.zsh
	_vbe_vcs_async_start() {
		async_start_worker vcs_info
		async_register_callback vcs_info _vbe_vcs_info_done
	}
	_vbe_vcs_info() {
		cd -q $1
		vcs_info
		print ${vcs_info_msg_0_}
	}
	_vbe_vcs_info_done() {
		local job=$1
		local return_code=$2
		local stdout=$3
		local more=$6
		if [[ $job == '[async]' ]]; then
			if [[ $return_code -eq 2 ]]; then
				_vbe_vcs_async_start
				return
			fi
		fi
		vcs_info_msg_0_=$stdout
		[[ $more == 1 ]] || zle reset-prompt
	}
	source $HOME/.zsh/plugins/zsh-async/async.zsh
	async_init
	_vbe_vcs_async_start
	add-zsh-hook precmd (){
		async_job vcs_info _vbe_vcs_info $PWD
	}
	add-zsh-hook chpwd (){
		vcs_info_msg_0_=
	}
else
	# synchronous vcs info
	precmd_vcs_info() { vcs_info }
	precmd_functions+=( precmd_vcs_info )
fi
setopt prompt_subst

if [ -e $HOME/.zsh/plugins/op.zsh ]
then
	source $HOME/.zsh/plugins/op.zsh
fi

########### general settings
HISTFILE=~/.histfile
HISTSIZE=500000
SAVEHIST=10000
setopt APPEND_HISTORY		# append to history file, don't overwrite
unsetopt SHARE_HISTORY		# share the history between different sessions
setopt HIST_IGNORE_DUPS		# ignore duplicates in history
setopt HIST_IGNORE_SPACE	# ignore commands starting with space
setopt INC_APPEND_HISTORY 	# write history entries immediately
setopt EXTENDED_HISTORY         # puts timestamps in the history

unsetopt BEEP			# beep
setopt NOMATCH			# Print an error if pattern has no match
unsetopt AUTO_CD		# change directory without cd
unsetopt EXTENDED_GLOB		# Treat # ~ and ^ as part of patterns for filename generation
unsetopt NOTIFY			# Report the status of background jobs immediately
setopt AUTO_PUSHD		# push the old directory to the stack when using cd

#setopt CORRECT			# correct spelling of commands
#setopt CORRECT_ALL		# correct spelling of arguments

unsetopt RM_STAR_SILENT		# do not query the user before executing 'rm *'

# Prevent overwriting files
set -o noclobber

############# key-bindings
bindkey -e						# emacs editing style

bindkey "^[[2~" yank					# Insert
bindkey "^[[3~" delete-char				# Del
bindkey "^[[5~" history-beginning-search-backward	# PageUp
bindkey "^[[6~" history-beginning-search-forward	# PageDown
case "$TERM" in
	linux|screen*|tmux*)
		bindkey "^[[1~" beginning-of-line	# Pos1
		bindkey "^[[4~" end-of-line		# End
		bindkey "^[[H"  beginning-of-line	# Pos1
		bindkey "^[[F"  end-of-line		# End
		;;
	*xterm*|(dt|k)term)
		bindkey "^[[H"  beginning-of-line	# Pos1
		bindkey "^[[F"  end-of-line		# End
		bindkey "^[[7~" beginning-of-line	# Pos1
		bindkey "^[[8~" end-of-line		# End
		;;
	rxvt|Eterm)
		bindkey "^[[7~" beginning-of-line	# Pos1
		bindkey "^[[8~" end-of-line		# End
		;;
esac

############ find out operating system
case `uname -s` in
	FreeBSD)
		OPERATINGSYSTEM="BSD"
		;;
	*)
		OPERATINGSYSTEM="LINUX"
		;;
esac

############ Select vim or nvim depending on what's available
VIM=vim
if which nvim >/dev/null 2>&1
then
	VIM=nvim
fi
export GIT_EDITOR=$VIM

############ alias
alias tree="tree --charset utf8 -C -N"
alias tree_ascii="tree --charset ascii -N"
if [ "$OPERATINGSYSTEM" = "BSD" ]
then
	alias la='ls -G -a'
	alias ll='ls -G -l -h'
	alias ls='ls -G'
else
	alias la='ls -a --color=auto'
	alias ll='ls -l -h --color=auto'
	alias ls='ls --color=auto'
fi
alias mv='mv -i'
alias rm='rm -I'
alias cp='cp -i'
alias grep='grep --color=auto'
alias gr='grep --line-number --color=auto -r \
	--exclude-dir=".svn" \
	--exclude-dir=".git" \
	--exclude=".*.swo" \
	--exclude=".*.swp" \
	--exclude="tags"'
alias gg='grep --line-number --color=auto -r \
	--include="*.S" \
	--include="*.c" \
	--include="*.h" \
	--include="*.cc" \
	--include="*.cpp" \
	--include="*.hpp" \
	--include="configure.ac"'
alias gy='grep --line-number --color=auto -r \
	--include="*.py" \
	--include="*.pm" \
	--include="wscript"'
alias grc='gr --color=always'
alias ggc='gg --color=always'
alias gyc='gy --color=always'
alias pdfgrepp='pdfgrep --page-number'
alias pmount='pmount -e'
alias ssh='ssh -Y'
alias now='date +%Y%m%d_%H%M%S'
alias rpmdate='LANG="en" date +"%a %b %d %H:%M:%S %Z %Y"'
alias day='date +%Y%m%d'
alias shortday='date +%y%m%d'
alias hd='od -Ax'
unalias beep >/dev/null 2>&1
if [ "$VIM" != "vim" ]
then
	alias vim="$VIM"
	alias vimdiff="$VIM -d"
fi
alias tmux2='tmux new -t 0'

############ zip single file
zipsingle()
{
	for filename in $@
	do
		local nosuffix=`echo $filename | sed -e 's/\....$//'`
		zip -r "${nosuffix}.zip" "${filename}"
	done
}

#libreoffice()
#{
#	FIRST="$1"
#	shift
#	if [ "$FIRST" != "" ]
#	then
#		FIRST=`cygpath -w "$FIRST"`
#	fi
#
#	/cygdrive/c/Program\ Files\ \(x86\)/LibreOffice\ 4/program/soffice.exe \
#		"$FIRST" "${@}"
#}
#alias dot='/cygdrive/c/Program\ Files\ \(x86\)/Graphviz2.30/bin/dot.exe'
#alias wingvim='/cygdrive/c/Program\ Files\ \(x86\)/Vim/vim74/gvim.exe -N'
#alias tortoisemerge='/cygdrive/c/Program\ Files/TortoiseSVN/bin/TortoiseMerge.exe'
#alias meld='/cygdrive/c/Program\ Files\ \(x86\)/Meld/meld/meld.exe'
#alias acroread='/cygdrive/c/Program\ Files\ \(x86\)/Adobe/Reader\ 11.0/Reader/AcroRd32.exe'
#alias gvim='gvim -N'
#alias vim='vim -N'
#alias libreoffice='/cygdrive/c/Program\ Files\ \(x86\)/LibreOffice\ 4.0/program/soffice.exe'
#alias svn='colorsvn'

############ color man output
man() {
	env \
	LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	LESS_TERMCAP_md=$(printf "\e[1;31m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;32m") \
	man "$@"
}

############ Firejail puts symlinks to /usr/local/bin
PATH="/usr/local/bin:$PATH"

############ Path
PATH="$PATH:/sbin:/usr/sbin"
PATH="$HOME/scripts:$PATH"
PATH="$PATH:$HOME/.local/bin"

############ Environment
export EDITOR="$VIM -N -f"
#export PATH="/usr/lib/colorgcc/bin:$PATH"
export PAGER="less -R"
export PYTHONDOCS="/usr/share/doc/python/html/"
#export PYTHONDOCS="/usr/share/doc/python2/html/"
export CTAGS="--extra=+f"

############ Host-Specific Environment
if [ "$HOSTNAME" = "" ]
then
	HOSTNAME=$HOST
fi
if [ "$HOSTNAME" = "" ]
then
	HOSTNAME=`hostname -f`
fi
export HOSTNAME

[[ $HOSTNAME == christian-pc* ]] && MYSYSTEM="privat-pc-linux"
[[ $HOSTNAME == christian-x230i* ]] && MYSYSTEM="privat-nb-linux"
[[ $HOSTNAME == christian-inspiron* ]] && MYSYSTEM="privat-nb-linux"
[[ $HOSTNAME == christian-surftab* ]] && MYSYSTEM="privat-tablet-linux"
[[ $HOSTNAME == christian-tab* ]] && MYSYSTEM="privat-tablet-linux"
[[ $HOSTNAME == mauderer-linux* ]] && MYSYSTEM="work-nb-linux"
[[ $HOSTNAME == mauderer-nb-li* ]] && MYSYSTEM="work-nb-linux"
[[ $HOSTNAME == MAUDERER-NB-LI* ]] && MYSYSTEM="work-nb-linux"
[[ $HOSTNAME == MAUDERER-TUX* ]] && MYSYSTEM="work-nb-linux"
[[ $HOSTNAME == dia* ]] && MYSYSTEM="privat-headless-linux"
[[ $HOSTNAME == lupus* ]] && MYSYSTEM="work-shared-headless-linux"

PROMPTCOLOR="yellow"

if [[ $MYSYSTEM == privat*linux ]]
then
  PROMPTCOLOR="green"
  if [[ $MYSYSTEM == privat-tablet-linux ]]
  then
    PROMPTCOLOR="36"
  fi

  export PATH="$PATH:/opt/bin/"
  export PATH="$PATH:/opt/arm-gcc/current/bin"

  #PATH="/home/christian/perl5/bin${PATH+:}${PATH}"; export PATH;
  #PERL5LIB="/home/christian/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
  #PERL_LOCAL_LIB_ROOT="/home/christian/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
  #PERL_MB_OPT="--install_base \"/home/christian/perl5\""; export PERL_MB_OPT;
  #PERL_MM_OPT="INSTALL_BASE=/home/christian/perl5"; export PERL_MM_OPT;

  #PATH="$PATH:/usr/bin/site_perl/"

  export OOO_FORCE_DESKTOP="kde4"

elif [[ $MYSYSTEM == work-*-linux ]]
then
  #export DISPLAY=:0
  PROMPTCOLOR="green"

  ############ Perl local libs
  export PERL_LOCAL_LIB_ROOT="$PERL_LOCAL_LIB_ROOT:/home/EB/christian_m/perl5";
  export PERL_MB_OPT="--install_base /home/EB/christian_m/perl5";
  export PERL_MM_OPT="INSTALL_BASE=/home/EB/christian_m/perl5";
  export PERL5LIB="/home/EB/christian_m/perl5/lib/perl5:$PERL5LIB";
  export PATH="/home/EB/christian_m/perl5/bin:$PATH";

  ############ Trace32-specific
  export PATH="$PATH:/home/EB/christian_m/t32/bin/pc_linux64"
  export T32SYS="/home/EB/christian_m/t32"
  export T32TMP="/tmp"
  export T32ID="T32"
  export ADOBE_PATH="/home/EB/christian_m/trace32/adobe_wrapper"

fi

if [[ $MYSYSTEM == *-headless-* ]]
then
  export EDITOR="vim"

fi

if [[ $MYSYSTEM == *-shared-* ]]
then
  PROMPTCOLOR="magenta"
  alias killall="echo 'You do not want to use killall here'"
fi

############ Prompt
MYID=`id -u`
[[ $MYID -eq 0 ]] && PROMPTCOLOR="red"
PROMPT="${PREPROMPT}%B%F{${PROMPTCOLOR}}%40<…<%d %F{blue}%T >%f%b "
RPROMPT="%B%F{black}%40<…<%n@%m%F{blue}\$vcs_info_msg_0_%f%b%(0?.. %B%F{red}(%?%)%b%f)"

#PATH="$PATH:/non-cygwin/bin"
